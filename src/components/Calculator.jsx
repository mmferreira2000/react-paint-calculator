import React, { useState } from "react";
import "./css/Calculator.css"
import Container from "@mui/material/Container";
import { Box } from "@mui/system";

export default function Calculator() {
  const [wall1, setWall1] = useState({"height":'', "width":'', "doors":'', "windows":''})
  const handleWall1 = (e) => {
    if (e.target.getAttribute('name')==='height1') {
      setWall1({"height": e.target.value, "width": wall1.width, "doors": wall1.doors, "windows": wall1.windows})
    } else if (e.target.getAttribute('name')==='width1') {
      setWall1({"height": wall1.height, "width": e.target.value, "doors": wall1.doors, "windows": wall1.windows})
    } else if (e.target.getAttribute('name')==='doors1') {
      setWall1({"height": wall1.height, "width": wall1.width, "doors": e.target.value, "windows": wall1.windows})
    } else if (e.target.getAttribute('name')==='windows1'){
      setWall1({"height": wall1.height, "width": wall1.width, "doors": wall1.doors, "windows": e.target.value})
    }
  }

  const [wall2, setWall2] = useState({"height":'', "width":'', "doors":'', "windows":''})
  const handleWall2 = (e) => {
    if (e.target.getAttribute('name')==='height2') {
      setWall2({"height": e.target.value, "width": wall2.width, "doors": wall2.doors, "windows": wall2.windows})
    } else if (e.target.getAttribute('name')==='width2') {
      setWall2({"height": wall2.height, "width": e.target.value, "doors": wall2.doors, "windows": wall2.windows})
    } else if (e.target.getAttribute('name')==='doors2') {
      setWall2({"height": wall2.height, "width": wall2.width, "doors": e.target.value, "windows": wall2.windows})
    } else if (e.target.getAttribute('name')==='windows2'){
      setWall2({"height": wall2.height, "width": wall2.width, "doors": wall2.doors, "windows": e.target.value})
    }
  }

  const [wall3, setWall3] = useState({"height":'', "width":'', "doors":'', "windows":''})
  const handleWall3 = (e) => {
    if (e.target.getAttribute('name')==='height3') {
      setWall3({"height": e.target.value, "width": wall3.width, "doors": wall3.doors, "windows": wall3.windows})
    } else if (e.target.getAttribute('name')==='width3') {
      setWall3({"height": wall3.height, "width": e.target.value, "doors": wall3.doors, "windows": wall3.windows})
    } else if (e.target.getAttribute('name')==='doors3') {
      setWall3({"height": wall3.height, "width": wall3.width, "doors": e.target.value, "windows": wall3.windows})
    } else if (e.target.getAttribute('name')==='windows3'){
      setWall3({"height": wall3.height, "width": wall3.width, "doors": wall3.doors, "windows": e.target.value})
    }
  }

  const [wall4, setWall4] = useState({"height":'', "width":'', "doors":'', "windows":''})
  const handleWall4 = (e) => {
    if (e.target.getAttribute('name')==='height4') {
      setWall4({"height": e.target.value, "width": wall4.width, "doors": wall4.doors, "windows": wall4.windows})
    } else if (e.target.getAttribute('name')==='width4') {
      setWall4({"height": wall4.height, "width": e.target.value, "doors": wall4.doors, "windows": wall4.windows})
    } else if (e.target.getAttribute('name')==='doors4') {
      setWall4({"height": wall4.height, "width": wall4.width, "doors": e.target.value, "windows": wall4.windows})
    } else if (e.target.getAttribute('name')==='windows4'){
      setWall4({"height": wall4.height, "width": wall4.width, "doors": wall4.doors, "windows": e.target.value})
    }
  }

  const [result, setResult] = useState('')

  function calc () {
    const walls = [wall1, wall2, wall3, wall4]
    for (var i = 0; i < walls.length; i++) {
      const element = walls[i];
      if (element.height < 1 || element.height > 50 ) {
        alert("BatParedes deve ter entre 1m² e 50m².")
        window.location.reload()
        break;
      } else if (element.doors > 0 && element.height < 2.2) {
        alert("BatParedes com BatPortas devem ter no mínimo 2.20m de altura.")
        window.location.reload()
        break;
      } else if (element.doors + element.windows > i / 2) {
        alert("BatPortas e BatJanelas não podem ocupar mais de 50% da parede.")
        window.location.reload()
        break;
      }
    }

    const total1 = wall1.height * wall1.width - ((wall1.doors * 1.52) + (wall1.windows * 2.40))
    const total2 = wall2.height * wall2.width - ((wall2.doors * 1.52) + (wall2.windows * 2.40))
    const total3 = wall3.height * wall3.width - ((wall3.doors * 1.52) + (wall3.windows * 2.40))
    const total4 = wall4.height * wall4.width - ((wall4.doors * 1.52) + (wall4.windows * 2.40))

    var area = total1 + total2 + total3 + total4
    var liters = Math.ceil(area / 5)

    const cans = [18, 3.6, 2.5, 0.5];
    const result = [];
    let rest = liters.toFixed(1);

    for (let value of cans) {
      const quantity = Math.ceil(rest / value);
      if (quantity === 0) continue;
      result.push({ value, quantity });
      rest = rest % value;
    }

    let total = result.map(function(can, indice){
        const value = can.value
        const quantity = can.quantity
        return [`${quantity} BatLata(s) de ${value}L` ]
    });
    setResult(`Sugestão de compra: ${total}`)
  }

  return (
    <div>
      <Box m={2}/>
      <Container maxWidth='md'>
        <div className="wrapper">
          <h1>BatCalculator®</h1>
          <Box m={10}/>
          <p className='p-visible'>Parede 1</p>
          <div className="wall" id="1">
            <div className="form__group field">
                <input placeholder="Name" name="height1" onChange={(e) => {handleWall1(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="height1">Altura</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="width1" onChange={(e) => {handleWall1(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="width1">Comprimento</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="doors1" onChange={(e) => {handleWall1(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="doors1">Portas</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="windows1" onChange={(e) => {handleWall1(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="windows1">Janelas</label>
            </div>
          </div>
          <p className='p-visible'>Parede 2</p>
          <div className="wall" id="2">
            <div className="form__group field">
                <input placeholder="Name" name="height2" onChange={(e) => {handleWall2(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="height2">Altura</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="width2" onChange={(e) => {handleWall2(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="width2">Comprimento</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="doors2" onChange={(e) => {handleWall2(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="doors2">Portas</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="windows2" onChange={(e) => {handleWall2(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="windows2">Janelas</label>
            </div>
          </div>
          <p className='p-visible'>Parede 3</p>
          <div className="wall" id="3">
            <div className="form__group field">
                <input placeholder="Name" name="height3" onChange={(e) => {handleWall3(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="height3">Altura</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="width3" onChange={(e) => {handleWall3(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="width3">Comprimento</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="doors3" onChange={(e) => {handleWall3(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="doors3">Portas</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="windows3" onChange={(e) => {handleWall3(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="windows3">Janelas</label>
            </div>
          </div>
          <p className='p-visible'>Parede 4</p>
          <div className="wall" id="4">
            <div className="form__group field">
                <input placeholder="Name" name="height4" onChange={(e) => {handleWall4(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="height4">Altura</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="width4" onChange={(e) => {handleWall4(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="width4">Comprimento</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="doors4" onChange={(e) => {handleWall4(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="doors4">Portas</label>
            </div>
            <div className="form__group field">
                <input placeholder="Name" name="windows4" onChange={(e) => {handleWall4(e)}} className="form__field" type="number" min='0'/>
                <label className="form__label" for="windows4">Janelas</label>
            </div>
          </div>
          <Box m={10}/>
          <div className="result">
            {result}
          </div>
          <Box m={10}/>
          <div className="calc">
            <button onClick={calc}> <span> Bat Calcular </span></button>
          </div>
        </div>
      </Container>
    </div>
  )
}
